import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFlieImplService } from './service/students-flie-impl.service';
import { StudentsPeopleImplService } from './service/students-people-impl.service';


@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule,FormsModule,HttpClientModule
  ],
  providers: [
    {provide: StudentService, useClass: StudentsPeopleImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
