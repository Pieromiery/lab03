import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import { Student } from '../entity/student';
@Injectable({
  providedIn: 'root'
})
export class StudentDataImplService extends StudentService{

  constructor() {
    super();
  }
  getStudents(): Observable<Student[]>{
    return of(this.students);
  };

  students: Student[] = [{
    'id': 1,
    'studentId': '562110507',
    'name': 'Prayuth',
    'surname': 'Tu',
    'image': "/assets/images/tu.jpg",
    "featured": true,
    'gpa': 4.00,
    'penAmount': 0
  }, {
    'id': 2,
    'studentId': '562110509',
    'name': 'Pu',
    'surname': 'Priya',
    'image': "assets/images/pu.jpg",
    "featured": false,
    'gpa': 2.00,
    'penAmount':0
  }];
}
